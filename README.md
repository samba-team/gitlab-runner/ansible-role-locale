# ansible-role-locale

Ansible Role to setup locale.

## Installation

Add to `requirements.yml`:

    - src: git+https://github.com/catalyst-samba/ansible-role-locale.git
      name: locale

Install:

    ansible-galaxy install -f -r requirements.yml

This will install role into `~/.ansible/roles`. You can also link the repo to there.

## Usage

The role will set locale to `en_US.utf8` by default:

    - name: setup locale
      hosts: localhost
      tasks:
        - name: include role locale
          include_role:
            name: locale

For a different locale:

    - name: setup locale to zh_CN.utf8
      hosts: localhost
      tasks:
        - name: include role locale
          include_role:
            name: locale
          vars:
            locale_charmap: UTF-8
            locale_inpoutfile: zh_CN
            locale_name: zh_CN.utf8

NOTE: glibc/locales/localedef will normalize `UTF-8` to `utf8`. Please use `utf8` in `locale_name` to follow the naming style.


## Tested distributions

- Debian 7, 8, 9
- Ubuntu 14.04, 16.04, 18.04
- CentOS 6, 7
- Fedora 28, 29
